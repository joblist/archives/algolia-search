import MapList from './list.js'

const companiesResultsToMapMarkers = (searchResults = []) => {
	const companyMarkers = []
	searchResults.forEach(result => {
		if (!result.positions) return
		result.positions.forEach(position => {
			if (!position.map) return
			let mapData
			try {
				mapData = JSON.parse(position.map)
			} catch (error) {
				console.log('erorr parsing map data', error)
			}
			if (
				mapData
				&& mapData.type === 'Point'
				&& mapData.coordinates.length === 2
			) {
				companyMarkers.push({
					text: result.title,
					slug: result.slug,
					longitude: parseFloat(mapData.coordinates[0]),
					latitude: parseFloat(mapData.coordinates[1]),
				})
			}
		})
	})
	return companyMarkers
}

const companiesFileToMapMarkers = (({ edges }) => {
	let markers = []
	edges.forEach(({ node }) => {
		markers = [...markers, ...companyFileToMapMarkers(node)]

	})
	return markers
})

const companyFileToMapMarkers = (companyNode) => {
	const { frontmatter, fields } = companyNode
	if (!frontmatter.positions) {
		/* no markers for this company */
		return
	}
	let markers = []
	frontmatter.positions.forEach(position => {
		if (!position.map) return
		let mapData = null
		try {
			mapData = JSON.parse(position.map)
		} catch (error) {}
		if (!mapData
			|| !mapData.coordinates.length === 2
			|| !mapData.type === 'Point'
		) return

		const marker = {
			text: frontmatter.title,
			slug: fields.slug,
			longitude: mapData.coordinates[0],
			latitude: mapData.coordinates[1],
		}
		markers.push(marker)
	})
	return markers
}

export {
	MapList,
	companiesResultsToMapMarkers,
	companiesFileToMapMarkers,
}
