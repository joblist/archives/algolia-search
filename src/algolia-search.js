class AlgoliaSearch extends HTMLElement {
	get $search() {
		return this.querySelector('#SearchBox input')
	}
	get searchQuery() {
		if (this.$search) {
			return this.$search.value
		} else {
			return ''
		}
	}
	get origin() {
		return this.getAttribute('origin') || 'https://profiles.joblist.today'
	}
	/* algolia */
	get indexNames() {
		const namesAttr = this.getAttribute('index-names')
		let names = []
		if (namesAttr) {
			names = namesAttr.split(',')
		}
		return names
	}
	get appId() {
		return this.getAttribute('app-id')
	}
	get apiKey() {
		return this.getAttribute('api-key')
	}
	get routing() {
		return this.getAttribute('routing') === 'true'
	}
	get showReset() {
		return this.getAttribute('show-reset') === 'true'
	}
	get placeholder() {
		const placeholder = 'Search for jobs, by name, city, tag...'
		return this.getAttribute('placeholder') || placeholder
	}
	get refinementAttribute() {
		return this.getAttribute('refinement-attribute')
	}
	get refinementValues() {
		return this.getAttribute('refinement-values') ? this.getAttribute('refinement-values').split('/') : []
	}
	get autofocus() {
		return this.getAttribute('autofocus') === 'true'
	}
	/* is the current host url the same as origin? */
	get sameOrigin() {
		return this.origin === window.origin
	}

	get utmSource() {
		return this.getAttribute('utm-source') || 'joblist.today'
	}
	get utmCampaign() {
		return this.getAttribute('utm-campaign') || 'search.joblist.today'
	}

	get widgetHitsConfig() {
		const $componentRef = this
		const onSearchResults = this.onSearchResults.bind(this)
		const { utmSource, utmCampaign } = this
		const config = {}
		config.prod_jobs = {
			templates: {
				empty() {
					return 'No result in Jobs'
				},
				item(hit, { html, components }) {
					const hitUrl = new URL(hit.url)
					hitUrl.searchParams.set('utm_source', utmSource)
					hitUrl.searchParams.set('utm_campaign', utmCampaign)
					return html`
						<article class='Result Result--job'>
							<a class='Result-company' href='${$componentRef.origin}/companies/${hit.companySlug}'>${components.Highlight({ hit, attribute: 'companyTitle' })}</a>
							<a class='Result-url' href='${hitUrl}'>${components.Highlight({ hit, attribute: 'name' })}</a>
							<span class='Result-location'>${components.Highlight({ hit, attribute: 'location' })}</span>
						</article>
					`;
				},
			},
			transformItems(items) {
				onSearchResults('prod_jobs', items)
				return items
			},
		}
		config.prod_companies = {
			templates: {
				empty() {
					return 'No result in Companies'
				},
				item(hit, { html, components }) {
					return html`
						<article class="Result Result--company">
							<a class='Result-name' href='${$componentRef.origin}/companies/${hit.slug}'>${components.Highlight({ hit, attribute: 'title' })}</a>
							<span class='Result-tags'>
								${hit.tags.map((tag) => (
								html`<a class='Result-tag' href='${$componentRef.origin}/tags/companies/${tag}'>${tag}</a>`
							))}
							</span>
							<span class='Result-positions'>
								${hit.positions && hit.positions.map((position) => {
						return html`<span class='Result-position'>${position.city}</span>`
					})}
							</span>
						</article>
					`;
				},
			},

			/* a trick to send search results outside this component */
			transformItems(items) {
				onSearchResults('prod_companies', items)
				return items
			},
		}
		return config
	}

	constructor() {
		super()
	}

	connectedCallback() {
		if (this.appId && this.apiKey) {
			this.searchClient = algoliasearch(this.appId, this.apiKey)
			this.render()
			this.initSearch()
			this.bindSearch()
		}
	}

	refreshAdsenseResults() {
		pageOptions.query = lastQuery;
		if (lastQuery) {
			_googCsa('ads', pageOptions, adblock);
		}
	}

	/* send an event above, so other non-algolia widgets can display the results (map) */
	onSearchResults(indexName, items) {
		/* send an event  */
		const eventContext = new CustomEvent('algoliaSearchResult', {
			bubbles: true,
			detail: {
				indexName,
				searchQuery: this.searchQuery,
				items,
			}
		})
		this.dispatchEvent(eventContext)
	}

	render() {
		/* algolia elements */
		const $searchBox = document.createElement('header')
		$searchBox.setAttribute('id', 'SearchBox')

		const $refinementList = document.createElement('div')
		$refinementList.setAttribute('id', 'SearchRefinementList')

		const $currentRefinementList = document.createElement('div')
		$currentRefinementList.setAttribute('id', 'SearchCurrentRefinementList')

		/* indicices */
		const $searchHitsIndicices = document.createElement('main')
		$searchHitsIndicices.setAttribute('id', `SearchHits`)
		/* search results, for all indices (main and subs) */
		if (this.indexNames && this.indexNames.length) {
			this.indexNames.forEach((indexName, indexIndex) => {
				const $searchHitsIndex = document.createElement('section')
				$searchHitsIndex.setAttribute('id', `SearchHitsIndex-${indexName}`)
				$searchHitsIndex.classList.add('SearchHitsIndex', `SearchHitsIndex-${indexName}`)

				const $searchHitsIndexTitle = document.createElement('h2')
				$searchHitsIndexTitle.classList.add('SearchHits-title')
				const indexTitleParts = indexName.split('_')
				const indexTitle = indexTitleParts[indexTitleParts.length - 1]
				$searchHitsIndexTitle.innerText = indexTitle

				const $searchHitsIndexContainer = document.createElement('main')
				$searchHitsIndexContainer.classList.add('SearchHits-main')

				$searchHitsIndex.append($searchHitsIndexTitle)
				$searchHitsIndex.append($searchHitsIndexContainer)

				/* the first index being the main, only show navigation there */
				if (indexIndex === 0) {
					/* pagination */
					const $searchPagination = document.createElement('div')
					$searchPagination.setAttribute('id', 'SearchPagination')
					$searchHitsIndex.append($searchPagination)
				}

				/* append each index to the indices list */
				$searchHitsIndicices.append($searchHitsIndex)
			})
		}
		/* insert all element in the right order */
		this.append($searchBox)
		this.append($searchHitsIndicices)
		this.append($refinementList)
		this.append($currentRefinementList)
	}

	initSubIndices(indices) {
		return indices.map((indexName) => {
			let widgetHitsConfig = this.widgetHitsConfig[indexName] || {}
			widgetHitsConfig.container = `#SearchHitsIndex-${indexName} main`

			const subIndex = instantsearch({
				searchClient: this.searchClient,
				indexName: indexName,
				routing: this.routing,
			})

			subIndex.addWidgets([
				instantsearch.widgets.configure({
					hitsPerPage: 5,
				}),
				instantsearch.widgets.hits(widgetHitsConfig),
			])

			return subIndex
		})
	}

	initMainIndex(indexName, subIndices) {
		let widgetHitsConfig = this.widgetHitsConfig[indexName] || {}
		widgetHitsConfig.container = `#SearchHitsIndex-${indexName} main`

		const mainIndex = instantsearch({
			searchClient: this.searchClient,
			indexName: indexName,
			routing: this.routing,
			searchFunction: (helper) => {
				if (subIndices && subIndices.length) {
					subIndices.forEach(subIndex => {
						subIndex.helper.setQuery(helper.state.query).search()
					})
				}
				helper.search()
			},
		})

		mainIndex.addWidgets([
			instantsearch.widgets.configure({
				hitsPerPage: 15,
			}),
			instantsearch.widgets.hits(widgetHitsConfig),
			instantsearch.widgets.searchBox({
				container: '#SearchBox',
				placeholder: this.placeholder,
				showReset: this.showReset,
			}),
			instantsearch.widgets.pagination({
				container: '#SearchPagination',
				showFirst: false
			}),
		])
		return mainIndex
	}

	initSearch() {
		// NEW (multiple index)
		let subIndices = []
		if (this.indexNames && this.indexNames.length > 1) {
			subIndices = this.initSubIndices(
				this.indexNames.slice(1, this.indexNames.length)
			)
		}

		let mainIndex = this.initMainIndex(
			this.indexNames[0],
			subIndices,
		)

		/* start all indexes (the `main` one last) */
		let indices = [mainIndex]
		if (subIndices.length) {
			indices = [...indices, ...subIndices]
		}
		indices.reverse().forEach(index => index.start())

		if (this.autofocus) {
			this.querySelector('#SearchBox input').focus()
		}
	}

	/* algolia search is already handled by their widgets (input) */
	bindSearch() {
		if (this.$search) {
			this.$search.addEventListener('input', this.onSearchInput.bind(this))
		}
	}
	onSearchInput() {
		const searchEvent = new CustomEvent('algoliaSearchQuery', {
			bubbles: true,
			detail: {
				indexNames: this.indexNames,
				searchQuery: this.searchQuery,
			}
		})
		this.dispatchEvent(searchEvent)
	}
}

export default AlgoliaSearch
