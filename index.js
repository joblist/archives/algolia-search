import AlgoliaSearch from './src/algolia-search.js'

/* insert algolia & instantsearch scripts */
const createAlgoliaScript = async () => {
	try {
		await new Promise(async (resolve) => {
			const algoliaSearch = document.createElement('script')
			algoliaSearch.src = 'https://cdn.jsdelivr.net/npm/algoliasearch@4.14.3/dist/algoliasearch-lite.umd.js'
			algoliaSearch.async = true
			algoliaSearch.onload = resolve
			document.body.appendChild(algoliaSearch)
		})
	} catch (error) {
		console.log('Error inserting alogliaSearch script', error)
		return false
	}

	try {
		await new Promise(async (resolve) => {
			const algoliaInstantSearch = document.createElement('script')
			algoliaInstantSearch.src = 'https://cdn.jsdelivr.net/npm/instantsearch.js@4.49.2/dist/instantsearch.production.min.js'
			algoliaInstantSearch.async = true
			algoliaInstantSearch.onload = resolve
			document.body.appendChild(algoliaInstantSearch)
		})
	} catch (error) {
		console.log('Error inserting alogliaInstantSearch script', error)
		return false
	}

	return true
}

const createLeafletScripts = async () => {
	/* styles are inserted by the component, because it uses shadow dom */
	/*
		 <script src="https://unpkg.com/leaflet@1.9.2/dist/leaflet.js" integrity="sha256-o9N1jGDZrf5tS+Ft4gbIK7mYMipq9lqpVJ91xHSyKhg=" crossorigin=""></script>
	 */

	try {
		await new Promise(async (resolve) => {
			const $leafletScript = document.createElement('script')
			$leafletScript.src = 'https://unpkg.com/leaflet@1.9.2/dist/leaflet.js'
			$leafletScript.async = true
			$leafletScript.onload = resolve
			$leafletScript.setAttribute('integrity', 'sha256-o9N1jGDZrf5tS+Ft4gbIK7mYMipq9lqpVJ91xHSyKhg=')
			$leafletScript.setAttribute('crossorigin', '')
			document.body.appendChild($leafletScript)
		})
	} catch (error) {
		console.log('Error inserting leaflet script', error)
		return false
	}
}

/* customElements.define('algolia-search', AlgoliaSearch) */
export {
	/* initial script import functions */
	createAlgoliaScript,
	createLeafletScripts,

	/* custom elements */
	AlgoliaSearch,
}
